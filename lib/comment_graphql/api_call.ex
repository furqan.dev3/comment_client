defmodule CommentGraphql.CallApi do

  def get_user_name(comment_map) do
    # IO.inspect comment_map
    case comment_map do
      [] ->
        nil

      "" ->
        nil
      _ ->
        data_ =
        for data <- comment_map do

          # IO.inspect data, label: "from for"
          id = data.userid

          # IO.inspect id.userid

          Neuron.Config.set(url: "http://3.141.170.142:5091/graphiql")

          case Neuron.query("""
            {
              profile(authId:"",
              id:"#{id}") {
                fullname
                profilePhoto
                username
              }
            }
            """) do
            {:ok, response} ->
              fullname = response.body["data"]["profile"]["fullname"]
              pic = response.body["data"]["profile"]["profilePhoto"]
              username = response.body["data"]["profile"]["username"]
              comment_map = Map.merge(data, %{fullname: fullname, profile_photo: pic, username: username})
              comment_map
            {:error, _response} ->
              fullname = "profile module is not working"
              pic = "profile module is not working"
              username = "profile module is not working"
              comment_map = Map.merge(data, %{fullname: fullname, profile_photo: pic, username: username})
              comment_map
          end
        end
        data_
    end
  end
end
